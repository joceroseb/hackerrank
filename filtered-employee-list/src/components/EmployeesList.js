import React, {Fragment} from 'react';

class EmployeesList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        employeeList: ''
      }
    }
  render() {
    const { employees } = this.props;
    return (
      <Fragment>
        <div className="controls">
          <input type="text" className="filter-input" data-testid="filter-input" onChange={(e) => {(e.target.value);this.setState({employeeList: e.target.value})}}/>
        </div>
               <ul className="employees-list">
         {employees.filter((employee) => employee.name.toLowerCase().includes(this.state.employeeList.toLowerCase())).map(employee => (
            <li key={employee.name} data-testid="employee">{employee.name}</li>
        ))}
        </ul>
      </Fragment>
    );
  }
}

export default EmployeesList;
